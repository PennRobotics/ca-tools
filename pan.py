# TODO

# TODO: to be honest, reversing is probably the easiest way to figure out the schema here, although the .PAN files have structured segments


import argparse
from PIL import Image
import struct

BytesAsHex = lambda b: ''.join([('0' if v[0] < 0x10 else '') + hex(v[0])[2:] for v in struct.iter_unpack('B', b)])

class Pan:
    palette = [ 35, 0  ,  55,  # Transparent  ( TODO magenta )
               0  , 0  , 170,
               0  , 170, 0  ,
               0  , 170, 170,
               170, 0  , 0  ,
               0  , 0  , 0  ,  # Opaque black
               170, 85 , 0  ,
               170, 170, 170,
               85 , 85 , 85 ,
               85 , 85 , 255,
               85 , 255, 85 ,
               85 , 255, 255,
               255, 85 , 85 ,
               255, 85 , 255,
               255, 255, 85 ,
               255, 255, 255] + [0,] * (255-16) * 3
    MAX_LZW_BITS = 0xB

    def __init__(self, pan_data):
        self._pan_data = pan_data
        self._p = 0

        tag = ''.join([chr(self.GetUByte()) for _ in range(4)])
        assert 'PANI' == tag
        assert 0x03 == self.GetUByte()
        assert 0x01 == self.GetUByte()
        assert 0x01 == self.GetUByte()
        assert 0x00 == self.GetUByte()
        assert 0x03 == self.GetUByte()
        for _ in range(15):  # TODO: color mapping
            self.GetUByte()
        for _ in range(5):
            assert 0x00 == self.GetUByte()
        w = self.GetUShort()
        h = self.GetUShort()
        UNK1 = self.GetUShort()
        UNK2 = self.GetUByte()
        UNK3 = self.GetUShort()
        frames = self.GetUShort()
        assert 0xc8 == self.GetUShort()  # 200 dec
        UNK4 = self.GetByte()  # maybe lzw_bits
        if UNK4 != self.MAX_LZW_BITS:
            raise ValueError
            #raise ValueError(f'Expected max LZW dictionary bit width is {hex(self.MAX_LZW_BITS)} and actual was {hex(lzw_bits)}')
        print(f'w: {w}, h: {h}, {frames} frames')

    # TODO: nothing below this is updated from copy-paste
        return




        # LZW
        self.ResetLZWDict()

        prev_seq = MakeByte(self.GetLZWChar())
        lzw_array = prev_seq
        self._table.append(MakeByte(0) + prev_seq)
        while self._p < len(self._pan_data):

            if len(self._table) == 2**self._bw:
                self._bw += 1
                if self._bw > self.MAX_LZW_BITS:
                    self.ResetLZWDict()

            curr = self.GetLZWChar()

            if curr == len(self._table):
                seq = prev_seq + MakeByte(prev_seq[0])
                self._set |= set([seq])
                self._table.append(seq)
                lzw_array += seq
                curr_seq = self._table[curr]

            else:
                try:  # TODO-debug
                 curr_seq = self._table[curr]
                except:  # TODO-debug
                 print(len(self._table), curr)
                 #import sys
                 #sys.exit(2)
                 break  # TODO-debug
                seq = prev_seq + MakeByte(curr_seq[0])

                if seq not in self._set:
                    self._set |= set([seq])
                    self._table.append(seq)
                    lzw_array += curr_seq

            prev_seq = curr_seq


        # RLE and Split pixels
        array = bytes()
        debugarray = bytes()
        rle_countdown = 0
        i = 0
        while i < len(lzw_array) and len(array) < w * h:
            bipixel = lzw_array[i]
            if bipixel == 0x90:
                i += 1
                if (followup_char := lzw_array[i]) == 0x0:
                    repeat_bipixel = 0x90
                else:
                    rle_countdown = followup_char - 2
            else:
                repeat_bipixel = bipixel

            hi, lo = [(repeat_bipixel & 0xF0) >> 4], [repeat_bipixel & 0x0F]
            debugarray += MakeByte(hi[0] << 4 | lo[0])
            array += bytes(lo) + bytes(hi)
            while rle_countdown > 0:
                hi, lo = [(repeat_bipixel & 0xF0) >> 4], [repeat_bipixel & 0x0F]
                debugarray += MakeByte(hi[0] << 4 | lo[0])
                array += bytes(lo) + bytes(hi)
                rle_countdown -= 1
            i += 1

        ### print(BytesAsHex(debugarray))

        array += bytes([0] * (w * h - len(array)))  # TODO-debug

        im = Image.frombytes('P', (w, h), array)
        im.putpalette(self.palette)
        im.show()

    ### def ResetLZWDict(self):
    ###     self._bq = 0  # bit value queue (the unused part of the 8 bits completing a LZW Char)
    ###     self._bs = 0  # bits in bit value queue
    ###     self._bw = 9  # bits per LZW word (will vary between 9 and MAX)
    ###     self._table = [bytes([n]) for n in range(256)]
    ###     self._set = set(self._table)

    ### def GetLZWChar(self):
    ###     while self._bs < self._bw:
    ###         self._bq |= (self.GetUByte() << self._bs)
    ###         self._bs += 8
    ###     lzw_char = self._bq & (2**self._bw - 1)
    ###     self._bs -= self._bw
    ###     self._bq >>= self._bw
    ###     ### print(f'({hex(lzw_char)})')
    ###     return lzw_char

    def GetByte(self):
        L = 1
        self._p += L
        return struct.unpack('b', self._pan_data[self._p-L:self._p])[0]

    def GetUByte(self):
        L = 1
        self._p += L
        return struct.unpack('B', self._pan_data[self._p-L:self._p])[0]

    def GetUShort(self):
        L = 2
        self._p += L
        return struct.unpack('H', self._pan_data[self._p-L:self._p])[0]

    def GetShort(self):
        L = 2
        self._p += L
        return struct.unpack('h', self._pan_data[self._p-L:self._p])[0]


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
            prog='Covert Action PAN decoder',
            description='Displays MicroProse PAN files',
            epilog='by Brian Wright')
    parser.add_argument('filename')
    args = parser.parse_args()

    if args.filename.upper()[-4:] != '.PAN':
        raise ValueError('Filename should have PAN extension')

    with open(args.filename, 'rb') as fh:
        pan_data = fh.read()

    p = Pan(pan_data)

