import pic
import struct

FILENAME = './assets/CITIES.CAT'
#FILENAME = './assets/FINAL3.CAT'
#FILENAME = './assets/FINAL4.CAT'

GetStringN = lambda n: fh.read(n).decode('ascii').strip('\0')
GetUShort = lambda: struct.unpack('H', fh.read(2))[0]
GetShort = lambda: struct.unpack('h', fh.read(2))[0]
GetULong = lambda: struct.unpack('I', fh.read(4))[0]
GetLong = lambda: struct.unpack('i', fh.read(4))[0]

with open(FILENAME, 'rb') as fh:
    num_entries = GetUShort()

    pics = []
    for entry in range(1, 1 + num_entries):
        name = GetStringN(12)
        csum = GetULong()
        length = GetULong()
        offset = GetULong()

        pics.append((name, offset, length, csum))

    for n, o, l, _ in pics:
        print(f'{n}\n{"="*len(n)}')
        if o != fh.tell():
            raise IOError(f'Expected offset is {hex(o)} and actual was {hex(fh.tell())}')
        pic_data = fh.read(l)
        #print([('0' if byte < 0x10 else '') + hex(byte)[2:] for byte in pic_data])
        pic.Pic(pic_data)
        input('Enter to advance...')
        #break  # TODO-debug
