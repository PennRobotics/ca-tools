import argparse
from PIL import Image
import struct

BytesAsHex = lambda b: ''.join([('0' if v[0] < 0x10 else '') + hex(v[0])[2:] for v in struct.iter_unpack('B', b)])

class Pic:
    '''
    An LZW Char is 11 bits, so between 2 and 3 bytes must be read to create a full LZW Char:
      e.g. 0bPPPPPPP0 0b10100101 0b10NNNNNN is 3 bytes; P are prev bits, N are next bits
                    ^   ^^^^^^^^   ^^
    The unused bits from the previous, byte-aligned, 8-bit read are queued, so only 1 or 2 bytes
      will need to be read to complete an LZW Char starting with any number of queued bits.
    '''

    palette = [ 35, 0  ,  55,  # Transparent  ( TODO magenta )
               0  , 0  , 170,
               0  , 170, 0  ,
               0  , 170, 170,
               170, 0  , 0  ,
               0  , 0  , 0  ,  # Opaque black
               170, 85 , 0  ,
               170, 170, 170,
               85 , 85 , 85 ,
               85 , 85 , 255,
               85 , 255, 85 ,
               85 , 255, 255,
               255, 85 , 85 ,
               255, 85 , 255,
               255, 255, 85 ,
               255, 255, 255] + [0,] * (255-16) * 3
    MAX_LZW_BITS = 0xB

    def __init__(self, pic_data):
        self._pic_data = pic_data
        self._p = 0
        MakeByte = lambda c: bytes([c]) if c is not None else bytes()

        fmt = self.GetUShort()
        w = self.GetUShort()
        h = self.GetUShort()
        if fmt == 0xF:
            for cmap in range(16):
                self.GetByte()  # TODO: color mapping (for CGA?)
        lzw_bits = self.GetByte()
        if lzw_bits != self.MAX_LZW_BITS:
            raise ValueError(f'Expected max LZW dictionary bit width is {hex(self.MAX_LZW_BITS)} and actual was {hex(lzw_bits)}')

        # LZW
        self.ResetLZWDict()

        prev_seq = MakeByte(self.GetLZWChar())
        lzw_array = prev_seq
        self._table.append(MakeByte(0) + prev_seq)
        while self._p < len(self._pic_data):

            if len(self._table) == 2**self._bw:
                self._bw += 1
                if self._bw > self.MAX_LZW_BITS:
                    self.ResetLZWDict()

            curr = self.GetLZWChar()

            if curr == len(self._table):
                seq = prev_seq + MakeByte(prev_seq[0])
                self._set |= set([seq])
                self._table.append(seq)
                lzw_array += seq
                curr_seq = self._table[curr]

            else:
                try:  # TODO-debug
                 curr_seq = self._table[curr]
                except:  # TODO-debug
                 print(len(self._table), curr)
                 #import sys
                 #sys.exit(2)
                 break  # TODO-debug
                seq = prev_seq + MakeByte(curr_seq[0])

                if seq not in self._set:
                    self._set |= set([seq])
                    self._table.append(seq)
                    lzw_array += curr_seq

            prev_seq = curr_seq


        # RLE and Split pixels
        array = bytes()
        debugarray = bytes()
        rle_countdown = 0
        i = 0
        while i < len(lzw_array) and len(array) < w * h:
            bipixel = lzw_array[i]
            if bipixel == 0x90:
                i += 1
                if (followup_char := lzw_array[i]) == 0x0:
                    repeat_bipixel = 0x90
                else:
                    rle_countdown = followup_char - 2
            else:
                repeat_bipixel = bipixel

            hi, lo = [(repeat_bipixel & 0xF0) >> 4], [repeat_bipixel & 0x0F]
            debugarray += MakeByte(hi[0] << 4 | lo[0])
            array += bytes(lo) + bytes(hi)
            while rle_countdown > 0:
                hi, lo = [(repeat_bipixel & 0xF0) >> 4], [repeat_bipixel & 0x0F]
                debugarray += MakeByte(hi[0] << 4 | lo[0])
                array += bytes(lo) + bytes(hi)
                rle_countdown -= 1
            i += 1

        ### print(BytesAsHex(debugarray))

        array += bytes([0] * (w * h - len(array)))  # TODO-debug

        im = Image.frombytes('P', (w, h), array)
        im.putpalette(self.palette)
        im.show()

    def ResetLZWDict(self):
        self._bq = 0  # bit value queue (the unused part of the 8 bits completing a LZW Char)
        self._bs = 0  # bits in bit value queue
        self._bw = 9  # bits per LZW word (will vary between 9 and MAX)
        self._table = [bytes([n]) for n in range(256)]
        self._set = set(self._table)

    def GetLZWChar(self):
        while self._bs < self._bw:
            self._bq |= (self.GetUByte() << self._bs)
            self._bs += 8
        lzw_char = self._bq & (2**self._bw - 1)
        self._bs -= self._bw
        self._bq >>= self._bw
        ### print(f'({hex(lzw_char)})')
        return lzw_char

    def GetByte(self):
        L = 1
        self._p += L
        return struct.unpack('b', self._pic_data[self._p-L:self._p])[0]

    def GetUByte(self):
        L = 1
        self._p += L
        return struct.unpack('B', self._pic_data[self._p-L:self._p])[0]

    def GetUShort(self):
        L = 2
        self._p += L
        return struct.unpack('H', self._pic_data[self._p-L:self._p])[0]

    def GetShort(self):
        L = 2
        self._p += L
        return struct.unpack('h', self._pic_data[self._p-L:self._p])[0]


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
            prog='Covert Action PIC decoder',
            description='Displays MicroProse PIC files',
            epilog='by Brian Wright')
    parser.add_argument('filename')
    args = parser.parse_args()

    if args.filename.upper()[-4:] != '.PIC':
        raise ValueError('Filename should have PIC extension')

    with open(args.filename, 'rb') as fh:
        pic_data = fh.read()

    p = Pic(pic_data)
